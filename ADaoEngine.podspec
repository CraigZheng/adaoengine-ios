#
# Be sure to run `pod lib lint ADaoEngine.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ADaoEngine'
  s.version          = '0.2.0'
  s.summary          = 'ADaoEngine - wrapper of API for A dao.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/CraigZheng/adaoengine-ios'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Haozheng Zheng' => 'haozheng.zheng@optus.com.au' }
  s.source           = { :git => 'https://CraigZheng@bitbucket.org/CraigZheng/adaoengine-ios.git', :tag => "0.2.0" }
  # s.social_media_url = 'http://www.weibo.com/u/3868827431/home?wvr=5'

  s.ios.deployment_target = '8.0'

  s.source_files = 'ADaoEngine/**/*'
  
  # s.resource_bundles = {
  #   'ADaoEngine' => ['ADaoEngine/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
