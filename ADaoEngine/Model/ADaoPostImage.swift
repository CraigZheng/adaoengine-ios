//
//  ADaoPostImage.swift
//  ADaoEngine
//
//  Created by Craig Zheng on 30/10/16.
//  Copyright © 2016 Craig. All rights reserved.
//

import UIKit

public class ADaoPostImage: NSObject {

    public var imageData: Data! // Must non-nil jpeg image.
    public var imageName: String! = "image"
    public var imageFormat: String! = "jpeg"
    public var shouldWatermark = false
    
}
