//
//  ADaoForumGroup.swift
//  ADaoEngine
//
//  Created by Craig Zheng on 28/10/16.
//  Copyright © 2016 Craig. All rights reserved.
//

import UIKit

public class ADaoForumGroup: NSObject {
    
    public var ID = 0
    public var sort = 0
    public var name: String?
    public var status: String?
    public var forums: Array<ADaoForum>?
    
    public convenience init(jsonDict: Dictionary<String, AnyObject>) {
        self.init()
        ID = Int(jsonDict["id"] as? String ?? "0") ?? 0
        sort = Int(jsonDict["sort"] as? String ?? "0") ?? 0
        name = jsonDict["name"] as? String
        status = jsonDict["status"] as? String
        if let forumsJSON = jsonDict["forums"] as? [Dictionary<String, AnyObject>] {
            forums = forumsJSON.map({ (jsonDict) -> ADaoForum in
                return ADaoForum(jsonDict: jsonDict)
            })
        }
    }

}

/*
 {
 "id": "6",
 "sort": "6",
 "name": "管理",
 "status": "n",
 "forums": [
 {
 "id": "18",
 "fgroup": "6",
 "sort": "1",
 "name": "值班室",
 "showName": "",
 "msg": "<p>&bull;本版发文间隔为15秒。<br />\r\n&bull;请在此举报不良内容，并附上串地址以及发言者ID。如果是回复串，请附上&ldquo;回应&rdquo;链接的地址，格式为&gt;&gt;No.串ID或&gt;&gt;No.回复ID<br />\r\n&bull;主站相关问题反馈、建议请在这里留言<br />\r\n&bull;已处理的举报将SAGE。</p>\r\n",
 "interval": "15",
 "createdAt": "2011-09-30 23:55:20",
 "updateAt": "2015-07-26 15:39:24",
 "status": "n"
 }
 ]
 }
 */
