//
//  ADaoForum.swift
//  ADaoEngine
//
//  Created by Craig on 28/10/16.
//  Copyright © 2016 Craig. All rights reserved.
//

import UIKit

public class ADaoForum: NSObject {
    public var ID = 0
    public var fgroup = 0
    public var sort = 0
    public var name: String?
    public var showName: String?
    public var message: String?
    public var interval = 0
    public var createdAt: Date?
    public var updatedAt: Date?
    public var status: String?

    public convenience init(jsonDict: Dictionary<String, AnyObject>) {
        self.init()
        // If cannot parse, set the ID to 0 instead.
        ID = Int(jsonDict["id"] as? String ?? "0") ?? 0
        fgroup = Int(jsonDict["fgroup"] as? String ?? "0") ?? 0
        sort = Int(jsonDict["sort"] as? String ?? "0") ?? 0
        name = jsonDict["name"] as? String
        showName = jsonDict["showName"] as? String
        message = jsonDict["msg"] as? String
        interval = Int(jsonDict["interval"] as? String ?? "0") ?? 0
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        createdAt = formatter.date(from: (jsonDict["createdAt"] as? String) ?? "")
        updatedAt = formatter.date(from: (jsonDict["updateAt"] as? String) ?? "")
        status = jsonDict["status"] as? String
    }
    
}
