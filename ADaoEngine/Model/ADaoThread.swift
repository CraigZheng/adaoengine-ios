//
//  ADaoThread.swift
//  ADaoEngine
//
//  Created by Craig on 28/10/16.
//  Copyright © 2016 Craig. All rights reserved.
//

import UIKit

public class ADaoThread: NSObject {
    
    public var responseCount = 0
    public var ID = 0
    public var userID: String?
    public var name: String?
    public var email: String?
    public var title: String?
    public var content: String?
    public var imageName: String?
    public var sage = false
    public var admin = false
    /// postDate is a string rather than Date object, why? Ask the backend guy.
    public var postDate: String?
    public var replies: Array<ADaoThread>?
    
    public convenience init(jsonDict: Dictionary<String, AnyObject>) {
        self.init()
        ID = Int(jsonDict["id"] as? String ?? "0") ?? 0 
        if let img = jsonDict["img"] as? String,
            let ext = jsonDict["ext"] as? String {
            imageName = img + ext
        }
        postDate = jsonDict["now"] as? String
        userID = jsonDict["userid"] as? String
        name = jsonDict["name"] as? String
        email = jsonDict["email"] as? String
        title = jsonDict["title"] as? String
        content = jsonDict["content"] as? String
        sage = jsonDict["sage"] as? Bool ?? false
        admin = jsonDict["admin"] as? Bool ?? false
        responseCount = Int(jsonDict["replyCount"] as? String ?? "0") ?? 0
        // Get replies, each entity is an ADaoThread object..
        if let repliesJSON = jsonDict["replys"] as? [Dictionary<String, AnyObject>], !repliesJSON.isEmpty {
            replies = repliesJSON.map({ (jsonDict) -> ADaoThread in
                return ADaoThread(jsonDict: jsonDict)
            })
        }
    }
    
}

/*
 
 "id":"9853552",
 "img":"",
 "ext":"",
 "now":"2016-09-22(四)21:31:57",
 "userid":"Ovear",
 "name":"无名氏",
 "email":"",
 "title":"无标题",
 "content":"",
 "sage":"1",
 "admin":"1",
 "remainReplys":238,
 "replyCount":"243",
 "replys":[]
 
 */
