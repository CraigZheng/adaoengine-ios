//
//  ADaoCookieEater.swift
//  ADaoEngine
//
//  Created by Craig Zheng on 30/10/16.
//  Copyright © 2016 Craig. All rights reserved.
//

import UIKit

/**
 Completion closure for ADaoCookieEater.updateCookies().
 - parameter success: the result of the dining expreience.
 - parameter cookies: an array of HTTPCookie objects returned by server.
 - parameter error: any error during the dining.
 */
public typealias CookieEaterCompletion = (_ success: Bool, _ cookies: [HTTPCookie]?, _ error: NSError?) -> Void

public class ADaoCookieEater: NSObject {
    
    static let getCookieEndpoint = "https://h.nimingban.com/Home/Api/getCookie?deviceid="
    
    /**
     Download cookies from server.
     - parameter completion: a completion that will be called when the downloading is completed.
     - returns: the URLSessionTask object that is created in this method, which will be responsible for downloading from the server. You can safely ignore this URLSessionTask object, or keep it so you can disconnect manually.
     */
    public static func updateCookies(completion: @escaping CookieEaterCompletion) -> URLSessionTask? {
        let cookieEndpoint = getCookieEndpoint.appending(UIDevice.current.identifierForVendor!.uuidString)
        let sessionTask: URLSessionTask?
        if let endpointURL = URL(string: cookieEndpoint) {
            var cookieRequest = URLRequest(url: endpointURL)
            // Should not handle cookie, should request with empty cookie header.
            cookieRequest.httpShouldHandleCookies = false
            sessionTask = URLSession(configuration: URLSessionConfiguration.default).dataTask(with: cookieRequest, completionHandler: { (data, response, error) in
                if let data = data,
                    let htmlString = String(data: data, encoding: .utf8),
                    let response = response as? HTTPURLResponse {
                    // Get http cookies.
                    let httpCookies: [HTTPCookie]?
                    if let responseHeaders = response.allHeaderFields as? [String: String],
                        let url = response.url {
                        httpCookies = HTTPCookie.cookies(withResponseHeaderFields: responseHeaders, for: url)
                    } else {
                        httpCookies = nil
                    }
                    completion(htmlString.lowercased().contains("ok"), httpCookies, nil)
                } else {
                    completion(false, nil, error as? NSError)
                }
            })
            sessionTask?.resume()
        } else {
            sessionTask = nil
        }
        
        return sessionTask
    }

}
