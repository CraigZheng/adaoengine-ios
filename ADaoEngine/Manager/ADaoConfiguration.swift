//
//  ADaoConfiguration.swift
//  ADaoEngine
//
//  Created by Craig Zheng on 30/10/16.
//  Copyright © 2016 Craig. All rights reserved.
//

import UIKit

public class ADaoConfiguration: NSObject {
    
    private struct Key {
        static let cookie = "Cookie"
        static let userAgent = "UserAgent"
    }

    /**
     Set the cookies to be used by ADaoEngine, default is nil.
     This method sets cookies for the shared HTTPCookieStorage object.
     */
    public var cookies: [HTTPCookie]? {
        set {
            if let newValue = newValue {
                newValue.forEach({ (cookie) in
                    HTTPCookieStorage.shared.setCookie(cookie)
                })
            }
        }
        get {
            return HTTPCookieStorage.shared.cookies
        }
    }
    
    /**
     Set the user agent of your app, it will be used in every network call made by ADaoEngine.
     The default value of this property is "ADAO_ENGINE_IOS".
     */
    public var userAgent: String! {
        set {
            if let newValue = newValue {
                UserDefaults.standard.set(newValue, forKey: Key.userAgent)
            } else {
                UserDefaults.standard.removeObject(forKey: Key.userAgent)
            }
            UserDefaults.standard.synchronize()
        }
        get {
            return (UserDefaults.standard.object(forKey: Key.userAgent) as? String) ?? "ADAO_ENGINE_IOS"
        }
    }
    
    public static let sharedInstance = ADaoConfiguration()
    
}
