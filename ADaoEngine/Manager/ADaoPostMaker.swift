//
//  ADaoPostMaker.swift
//  ADaoEngine
//
//  Created by Craig Zheng on 30/10/16.
//  Copyright © 2016 Craig. All rights reserved.
//

import UIKit

/**
 Completion closure for ADaoPostMaker.postNew() and ADaoPostmaker.reply().
 - parameter success: the result of the posting.
 - parameter responseHeaders: an array of headers returned by the server, would usually containt cookies values.
 - parameter responseHtml: the webpage response returned by server.
 - parameter error: any error during the posting.
 */
public typealias PostMakerCompletion = (_ success: Bool, _ responseHeads: [AnyHashable : AnyHashable]?, _ responseHtml: String?, _ error: NSError?) -> Void

public class ADaoPostMaker: NSObject {
    
    private struct Endpoint {
        static let postNew = "https://h.nimingban.com/Home/Forum/doPostThread.html"
        static let reply = "https://h.nimingban.com/Home/Forum/doReplyThread.html"
    }
    
    static let session = URLSession(configuration: URLSessionConfiguration.default)
    
    /**
     Post new content to a specified forum.
     - parameter thread: an ADaoThread object that contains messages that you would like to send, you can assign title, content, name and email to this thread object.
     - parameter forumID: the ID of the specified forum.
     - parameter image: an ADaoPostImage that represents an image, can be nil.
     - parameter completion: a completion closure to be called when the posting is completed.
     - returns: the URLSessionTask object that is created in this method, which will be responsible for downloading from the server. You can safely ignore this URLSessionTask object, or keep it so you can disconnect manually.
     */
    public static func postNew(thread: ADaoThread, to forumID: Int, with image: ADaoPostImage?, completion: @escaping PostMakerCompletion) -> URLSessionTask? {
        return post(with: thread, to: forumID, isReply: false, with: image, completion: completion)
    }
    
    /**
     Reply to a specified thread.
     - parameter thread: an ADaoThread object that contains messages that you would like to send, you can assign title, content, name and email to this thread object.
     - parameter threadID: the ID of the specified thread.
     - parameter: image: an ADaoPostImage that represents an image, can be nil.
     - parameter completion: a completion closure to be called when the posting is completed.
     - returns: the URLSessionTask object that is created in this method, which will be responsible for downloading from the server. You can safely ignore this URLSessionTask object, or keep it so you can disconnect manually.
     */
    public static func reply(with thread: ADaoThread, to threadID: Int, with image: ADaoPostImage?, completion: @escaping PostMakerCompletion) -> URLSessionTask? {
        return post(with: thread, to: threadID, isReply: true, with: image, completion: completion)
    }
    
    private static func post(with thread: ADaoThread, to ID: Int, isReply: Bool, with image: ADaoPostImage?, completion: @escaping PostMakerCompletion) -> URLSessionTask? {
        let sessionTask: URLSessionTask?
        let endpoint = isReply ? Endpoint.reply : Endpoint.postNew
        if let postURL = URL(string: endpoint) {
            var urlRequest = URLRequest(url: postURL)
            urlRequest.httpMethod = "POST"
            urlRequest.setValue("multipart/form-data; boundary=-0-x-K-h-T-m-L-b-O-u-N-d-A-r-Y-",
                                forHTTPHeaderField: "Content-Type")
            // Specify xml as return format.
            urlRequest.setValue("application/html", forHTTPHeaderField:"Accept")
            urlRequest.appendingADaoHeaders()
            var parameters = Dictionary<String, Any>()
            parameters[isReply ? "resto" : "fid"] = "\(ID)"
            parameters["title"] = thread.title ?? ""
            parameters["email"] = thread.email ?? ""
            parameters["name"] = thread.name ?? ""
            parameters["content"] = thread.content ?? ""
            // If image is included in this post, add watermark preference. The default watermark preference would be false.
            if let image = image {
                parameters["water"] = image.shouldWatermark ? "true" : "false"
            }
            // Assign these parameters to the request body.
            var requestBody: Data? = Data()
            let boundary = "-0-x-K-h-T-m-L-b-O-u-N-d-A-r-Y-" // Boundary can be any unique string.
            parameters.forEach({ (key, value) in
                requestBody?.append("--\(boundary)\r\n".data(using: .utf8)!)
                requestBody?.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
                requestBody?.append("\(value)\r\n".data(using: .utf8)!)
            })
            // If image is included,
            if let image = image {
                let data: Data?
                let format: String?
                if image.imageFormat.lowercased() == "gif" {
                    // When the incoming image is in gif format, don't perform any conversion.
                    data = image.imageData
                    format = "gif"
                } else if let jpgImage = UIImage(data: image.imageData),
                    let jpgImageData = UIImageJPEGRepresentation(jpgImage, 1.0) {
                    // When possible, convert all incoming image to jpg format.
                    data = jpgImageData
                    format = nil
                } else {
                    data = nil
                    format = nil
                }
                // If both data and format are initialised correctly, append them to the request body.
                if let data = data {
                    requestBody?.append("--\(boundary)\r\n".data(using: .utf8)!)
                    requestBody?.append("Content-Disposition: form-data; name=\"image\"; filename=\"\(image.imageName.lowercased()).\(format?.lowercased() ?? "jpg")\"\r\n".data(using: .utf8)!)
                    requestBody?.append("Content-Type: image/\(format?.lowercased() ?? "jpeg")\r\n\r\n".data(using: .utf8)!)
                    requestBody?.append(data)
                    requestBody?.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
                }
            }
            urlRequest.httpBody = requestBody
            sessionTask = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                if let data = data,
                    let htmlString = String(data: data, encoding: .utf8),
                    let urlResponse = response as? HTTPURLResponse,
                    error == nil {
                    completion(true, urlResponse.allHeaderFields as? [AnyHashable: AnyHashable], htmlString, nil)
                } else {
                    completion(false, nil, nil, error as? NSError)
                }
            })
            sessionTask?.resume()
        } else {
            sessionTask = nil
        }
        return sessionTask
    }
    
}
