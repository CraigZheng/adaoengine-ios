//
//  ADaoDownloader.swift
//  ADaoEngine
//
//  Created by Craig Zheng on 30/10/16.
//  Copyright © 2016 Craig. All rights reserved.
//

import UIKit

/**
 Completion closure for ADaoDownloader.updateForums().
 - parameter success: the result of the updating.
 - parameter forumGroups: an array of ADaoForumGroup objects.
 - parameter error: any error during the updating.
 */
public typealias ForumsUpdateCompletion = (_ success: Bool, _ forumGroups: [ADaoForumGroup]?, _ error: NSError?) -> Void
/**
 Completion closure for ADaoDownloader.downloadThreads().
 - parameter success: the result of the downloading.
 - parameter threads: an array of ADaoThread objects.
 - parameter error: any error during the updating.
 */
public typealias ThreadsUpdateCompletion = (_ success: Bool, _ threads: [ADaoThread]?, _ error: NSError?) -> Void
/**
 Completion closure for ADaoDownloader.downloadReplies().
 - parameter success: the result of the downloading.
 - parameter thread: the target thread from the server.
 - parameter replies: an array of ADaoThread objects, all replies of the target thread.
 - parameter error: any error during the updating.
 */
public typealias RepliesUpdateCompletion = (_ success: Bool, _ thread: ADaoThread?, _ replies: [ADaoThread]?, _ error: NSError?) -> Void
/**
 Completion closure for ADaoDownloader.downloadQuote().
 - parameter success: the result of the downloading.
 - parameter thread: the target thread from the server.
 - parameter error: any error during the updating.
 */
public typealias QuoteUpdateCompletion = (_ success: Bool, _ thread: ADaoThread?, _ error: NSError?) -> Void

/// The downloader class that you will be using to to download contents from server.
public class ADaoDownloader: NSObject {
    private struct Keyword {
        static let forumID = "<kForumID>"
        static let page = "<kPageNumber>"
        static let parent = "<kParentID>"
    }
    private struct Endpoint {
        static let forums = "https://h.nimingban.com/Api/getForumList"
        static let threads = "https://h.nimingban.com/Api/showf?id=<kForumID>&page=<kPageNumber>"
        static let replies = "https://h.nimingban.com/Api/thread?id=<kParentID>&page=<kPageNumber>"
        static let quote = "https://h.nimingban.com/Api/ref/id/<kParentID>"
    }
    
    static let session = URLSession(configuration: URLSessionConfiguration.default)

    /**
     Update forums with the server.
     - parameter completion: a completion that will be called when the updating is completed.
     - returns: the URLSessionTask object that is created in this method, which will be responsible for downloading from the server. You can safely ignore this URLSessionTask object, or keep it so you can disconnect manually.
    */
    public static func updateForums(completion: @escaping ForumsUpdateCompletion) -> URLSessionTask? {
        let sessionTask: URLSessionTask?
        if let forumURL = URL(string: Endpoint.forums) {
            var request = URLRequest(url: forumURL)
            request.appendingADaoHeaders()
            sessionTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let data = data,
                    let jsonDict = (try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)) as? [Dictionary<String, AnyObject>],
                    error == nil {
                    let forumGroups = jsonDict.map({ (jsonDict) -> ADaoForumGroup in
                        return ADaoForumGroup(jsonDict: jsonDict)
                    })
                    completion(true, forumGroups, nil)
                } else {
                    completion(false, nil, error as? NSError)
                }
            })
            sessionTask?.resume()
        } else {
            sessionTask = nil
        }
        return sessionTask
    }
    
    /**
     Download threads for a specified forum from server.
     - parameter forumID: the ID of the target forum.
     - parameter page: the page number, starting at 1.
     - parameter completion: a completion that will be called when the downloading is completed.
     - returns: the URLSessionTask object that is created in this method, which will be responsible for downloading from the server. You can safely ignore this URLSessionTask object, or keep it so you can disconnect manually.
     */
    public static func downloadThreads(for forumID: Int, at page: Int, completion: @escaping ThreadsUpdateCompletion) -> URLSessionTask? {
        let sessionTask: URLSessionTask?
        let threadsEndpoint = Endpoint.threads.replacingOccurrences(of: Keyword.forumID, with: "\(forumID)")
            .replacingOccurrences(of: Keyword.page, with: "\(page)")
        if let threadURL = URL(string: threadsEndpoint) {
            var request = URLRequest(url: threadURL)
            request.appendingADaoHeaders()
            sessionTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let data = data,
                    let jsonDict = (try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)) as? [Dictionary<String, AnyObject>],
                    error == nil {
                    let threads = jsonDict.map({ (jsonDict) -> ADaoThread in
                        return ADaoThread(jsonDict: jsonDict)
                    })
                    completion(true, threads, nil)
                } else {
                    completion(false, nil, error as? NSError)
                }
            })
            sessionTask?.resume()
        } else {
            sessionTask = nil
        }
        return sessionTask
    }
    
    /**
     Download replies for a specific thread from server.
     - parameter threadID: the ID of the target thread.
     - parameter page: the page number, starting at 1.
     - parameter completion: a completion that will be called when the downloading is completed.
     - returns: the URLSessionTask object that is created in this method, which will be responsible for downloading from the server. You can safely ignore this URLSessionTask object, or keep it so you can disconnect manually.
     */
    public static func downloadReplies(for threadID: Int, at page: Int, completion: @escaping RepliesUpdateCompletion) -> URLSessionTask? {
        let sessionTask: URLSessionTask?
        let repliesEndpoint = Endpoint.replies.replacingOccurrences(of: Keyword.parent, with: "\(threadID)")
            .replacingOccurrences(of: Keyword.page, with: "\(page)")
        if let repliesURL = URL(string: repliesEndpoint) {
            var request = URLRequest(url: repliesURL)
            request.appendingADaoHeaders()
            sessionTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let data = data,
                    let jsonDict = (try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)) as? Dictionary<String, AnyObject>,
                    error == nil {
                    let thread = ADaoThread(jsonDict: jsonDict)
                    completion(true, thread, thread.replies, nil)
                } else {
                    completion(false, nil, nil, error as? NSError)
                }
            })
            sessionTask?.resume()
        } else {
            sessionTask = nil
        }
        return sessionTask
    }
    
    /**
     Download content for a specified thread.
     - parameter threadID: the ID of the target thread.
     - parameter completion: a completion that will be called upon the downloading is completed.
     - returns: the URLSessionTask object that is created in this method, which will be responsible for downloading from the server. You can safely ignore this URLSessionTask object, or keep it so you can disconnect manually.
     */
    public static func downloadQuote(for threadID: Int, completion: @escaping QuoteUpdateCompletion) -> URLSessionTask? {
        let sessionTask: URLSessionTask?
        let quoteEndpoint = Endpoint.quote.replacingOccurrences(of: Keyword.parent, with: "\(threadID)")
        if let quoteURL = URL(string: quoteEndpoint) {
            var request = URLRequest(url: quoteURL)
            request.appendingADaoHeaders()
            sessionTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let data = data,
                    let jsonDict = (try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)) as? Dictionary<String, AnyObject>,
                    error == nil {
                    let thread = ADaoThread(jsonDict: jsonDict)
                    completion(true, thread, nil)
                } else {
                    completion(false, nil, error as? NSError)
                }
            })
            sessionTask?.resume()
        } else {
            sessionTask = nil
        }
        return sessionTask
    }
}
