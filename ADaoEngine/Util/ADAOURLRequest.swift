//
//  ADAOURLRequest.swift
//  ADaoEngine
//
//  Created by Craig Zheng on 30/10/16.
//  Copyright © 2016 Craig. All rights reserved.
//

import UIKit

extension URLRequest {

    mutating func appendingADaoHeaders() {
        var varSelf = self
        varSelf.setValue(ADaoConfiguration.sharedInstance.userAgent, forHTTPHeaderField: "User-Agent")
        self = varSelf
    }
    
}
