//
//  ADaoPostMakerTest.swift
//  ADaoEngine
//
//  Created by Craig Zheng on 30/10/16.
//  Copyright © 2016 Craig. All rights reserved.
//

import XCTest

import ADaoEngine

class ADaoPostMakerTest: XCTestCase {
    lazy var testThread: ADaoThread = {
        let thread = ADaoThread()
        thread.name = "肥贼测试中"
        thread.title = "肥贼测试中"
        thread.content = "肥贼测试中"
        thread.email = "肥贼测试中"
        
        return thread
    }()
    
    lazy var testImage: ADaoPostImage = {
        let postImage = ADaoPostImage()
        postImage.imageData = try! Data(contentsOf: Bundle(for: type(of: self)).url(forResource: "testImage", withExtension: "gif")!)
        postImage.imageName = "testImage"
        postImage.imageFormat = "gif"
        return postImage
    }()
    
    func testPostNew() {
        let expectation = self.expectation(description: "")
        let sessionTask = ADaoPostMaker.postNew(thread: testThread, to: 20, with: testImage, completion: {(success, responseHeads, responseHtml, error) in
            XCTAssert(success)
            XCTAssert(!responseHtml!.isEmpty)
            XCTAssert(error == nil)
            expectation.fulfill()
        })
        XCTAssertNotNil(sessionTask)
        waitForExpectations(timeout: 300, handler: nil)
    }
    
    func testReply() {
        let expectation = self.expectation(description: "")
        let sessionTask = ADaoPostMaker.reply(with: testThread, to: 10088110, with: testImage, completion: {(success, responseHeads, responseHtml, error) in
            XCTAssert(success)
            XCTAssert(!responseHtml!.isEmpty)
            XCTAssert(error == nil)
            expectation.fulfill()
        })
        XCTAssertNotNil(sessionTask)
        waitForExpectations(timeout: 300, handler: nil)
    }
    
    func testReplyTo() {
        
    }
}
