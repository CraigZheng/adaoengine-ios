//
//  ADaoCookieEaterTest.swift
//  ADaoEngine
//
//  Created by Craig Zheng on 30/10/16.
//  Copyright © 2016 Craig. All rights reserved.
//

import XCTest

import ADaoEngine

class ADaoCookieEaterTest: XCTestCase {

    func testCookieEater() {
        let expectation = self.expectation(description: "")
        let sessionTask = ADaoCookieEater.updateCookies { (success, cookies, error) in
            XCTAssert(error == nil)
            XCTAssert(success)
            XCTAssert(!(cookies?.isEmpty ?? true))
            ADaoConfiguration.sharedInstance.cookies = cookies
            expectation.fulfill()
        }
        XCTAssertNotNil(sessionTask)
        waitForExpectations(timeout: 300, handler: nil)
    }
    
}
