//
//  ADaoForumTest.swift
//  ADaoEngine
//
//  Created by Craig Zheng on 28/10/16.
//  Copyright © 2016 Craig. All rights reserved.
//

import XCTest

import ADaoEngine

class ADaoForumGroupTest: XCTestCase {
    lazy var jsonString: String = {
        try! String(contentsOfFile: Bundle(for:type(of:self)).path(forResource: "default_forums", ofType: "json")!)
    }()

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitForumGroups() {
        if let jsonDict = (try! JSONSerialization.jsonObject(with: (jsonString.data(using: .utf8))!, options: .mutableContainers)) as? [Dictionary<String, AnyObject>] {
            let forumGroups = jsonDict.map({ (jsonDict) -> ADaoForumGroup in
                return ADaoForumGroup(jsonDict: jsonDict)
            }).flatMap{$0}
            XCTAssert(forumGroups.count == 6)
            XCTAssert(forumGroups.first?.forums?.count == 20)
            XCTAssertNotNil(forumGroups.first?.ID)
            XCTAssertNotNil(forumGroups.first?.sort)
            XCTAssertNotNil(forumGroups.first?.name)
            XCTAssertNotNil(forumGroups.first?.status)
            
            if let firstForum = forumGroups.first?.forums?.first {
                XCTAssert(firstForum.ID == 4)
                XCTAssert(firstForum.fgroup == 4)
                XCTAssert(firstForum.sort == 3)
                XCTAssert(firstForum.name == "综合版1")
                XCTAssert(firstForum.showName == "")
                XCTAssert(!(firstForum.message?.isEmpty ?? true))
                XCTAssert(firstForum.interval == 30)
                XCTAssert(firstForum.status == "n")

                // The testing of dates would be done by revesing the formatting.
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                XCTAssert(formatter.string(from: firstForum.createdAt!) == "2011-10-21 15:49:28", formatter.string(from: firstForum.createdAt!))
                XCTAssert(formatter.string(from: firstForum.updatedAt!) == "2015-06-23 17:26:28", formatter.string(from: firstForum.updatedAt!))
            }
        }
        
    }
    /*
    "id": "4",
    "fgroup": "4",
    "sort": "3",
    "name": "综合版1",
    "showName": "",
    "msg": "<p><a href=\"http://h.nimingban.com/t/8156976\" target=\"_blank\"><img alt=\"种植AC娘~\" src=\"http://cover.acfunwiki.org/face.php\" style=\"float:left\" /></a></p>\r\n\r\n<p>&bull;欢迎光临，有关A岛的介绍请点&rarr;<a href=\"http://sx1.atv.ac:5566/index.php/%C4%E4%C3%FB%CC%D6%C2%DB%B0%E6\" target=\"_blank\">这里</a><br />\r\n<strong>&bull;<a href=\"https://h.nimingban.com/Forum\">全岛总版规</a>请仔细阅读，违者视规则删封砍</strong><br />\r\n&bull;请文明讨论，综合一不是<strong>垃圾场</strong>，无公共讨论意义/有碍公众浏览的内容请去<a href=\"https://h.nimingban.com/f/%E6%97%A5%E8%AE%B0\" target=\"_blank\">树洞版</a>，否则可能被SAGE<br />\r\n&bull;<a href=\"http://sx1.atv.ac:5566/index.php/SAGE\" target=\"_blank\">SAGE</a>并不意味着封禁，你可以继续在串里/左侧导航里寻找话题对应子版进行讨论</a><br />\r\n&bull;主站相关&rarr;<a href=\"/f/%E5%80%BC%E7%8F%AD%E5%AE%A4\">值班室</a>。宣传QQ群&rarr;<a href=\"http://sx1.atv.ac:5566/index.php/QQ\" target=\"_blank\">QQ群索引</a><br />\r\n&bull;<strong>丧尸们<a href=\"http://cdn.aixifan.com/h/mp3/tnnaii-h-island-c.mp3\">快来食我大雕啦</a>~</strong><br />\r\n&bull;本版发文间隔为30秒。</p>\r\n",
    "interval": "30",
    "createdAt": "2011-10-21 15:49:28",
    "updateAt": "2015-06-23 17:26:28",
    "status": "n"
    */
}
