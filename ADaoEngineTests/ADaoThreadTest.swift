//
//  ADaoThreadTest.swift
//  ADaoEngine
//
//  Created by Craig Zheng on 28/10/16.
//  Copyright © 2016 Craig. All rights reserved.
//

import XCTest

import ADaoEngine

class ADaoThreadTest: XCTestCase {
    
    lazy var jsonString: String = {
        try! String(contentsOfFile: Bundle(for:type(of:self)).path(forResource: "home", ofType: "json")!)
    }()
    
    lazy var jsonString2: String = {
        try! String(contentsOfFile: Bundle(for:type(of:self)).path(forResource: "thread", ofType: "json")!)
    }()
    
    func testInitADaoThreads() {
        if let jsonDict = (try! JSONSerialization.jsonObject(with: (jsonString.data(using: .utf8))!, options: .mutableContainers)) as? [Dictionary<String, AnyObject>] {
            let adaoThreads = jsonDict.map({ (jsonDict) -> ADaoThread in
                ADaoThread(jsonDict: jsonDict)
            })
            XCTAssert(adaoThreads.count == 20)
            // The third thread has 5 entities in the quick replies array.
            XCTAssert(adaoThreads[adaoThreads.startIndex.advanced(by: 2)].replies?.count == 5)
            XCTAssertNotNil(adaoThreads.first?.ID)
            XCTAssertNotNil(adaoThreads.first?.userID)
            XCTAssertNotNil(adaoThreads.first?.imageName)
            XCTAssertNotNil(adaoThreads.first?.postDate)
            XCTAssertNotNil(adaoThreads.first?.email)
            XCTAssertNotNil(adaoThreads.first?.sage)
            XCTAssertNotNil(adaoThreads.first?.admin)
            XCTAssertNotNil(adaoThreads.first?.content)
        }

    }
    
    func testInitRepliesThreads() {
        // The content of a thread is a json dictionary.
        if let jsonDict = (try! JSONSerialization.jsonObject(with: (jsonString2.data(using: .utf8))!, options: .mutableContainers)) as? Dictionary<String, AnyObject> {
            let adaoThread = ADaoThread(jsonDict: jsonDict)
            XCTAssert(adaoThread.replies?.count == 7)
            XCTAssert(adaoThread.ID == 10287544)
            XCTAssert(adaoThread.imageName == "2016-10-29/58140fae35940.png")
            XCTAssert(adaoThread.postDate == "2016-10-29(六)10:55:42")
            XCTAssert(adaoThread.userID == "NUOgL3v")
            XCTAssert(adaoThread.name == "无名氏")
            XCTAssert(adaoThread.email == "")
            XCTAssert(adaoThread.title == "无标题")
            XCTAssertNotNil(adaoThread.content)
            XCTAssert(adaoThread.responseCount == 26)
            XCTAssertFalse(adaoThread.sage)
            XCTAssertFalse(adaoThread.admin)
        }
        
    }
    
}
