//
//  ADaoDownloaderTest.swift
//  ADaoEngine
//
//  Created by Craig Zheng on 30/10/16.
//  Copyright © 2016 Craig. All rights reserved.
//

import XCTest

import ADaoEngine

class ADaoDownloaderTest: XCTestCase {
    
    func testUpdateForums() {
        let expectation = self.expectation(description: "")
        let sessionTask = ADaoDownloader.updateForums { (success, forumGroups, error) in
            XCTAssert(forumGroups?.count == 6)
            XCTAssert(forumGroups?.first?.forums?.count == 20)
            XCTAssertNotNil(forumGroups?.first?.ID)
            XCTAssertNotNil(forumGroups?.first?.sort)
            XCTAssertNotNil(forumGroups?.first?.name)
            XCTAssertNotNil(forumGroups?.first?.status)
            
            if let firstForum = forumGroups?.first?.forums?.first {
                XCTAssert(firstForum.ID == 4)
                XCTAssert(firstForum.fgroup == 4)
                XCTAssert(firstForum.sort == 3)
                XCTAssert(firstForum.name == "综合版1")
                XCTAssert(firstForum.showName == "")
                XCTAssert(!(firstForum.message?.isEmpty ?? true))
                XCTAssert(firstForum.interval == 30)
                XCTAssert(firstForum.status == "n")
                
                // The testing of dates would be done by revesing the formatting.
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                XCTAssert(formatter.string(from: firstForum.createdAt!) == "2011-10-21 15:49:28", formatter.string(from: firstForum.createdAt!))
                XCTAssert(formatter.string(from: firstForum.updatedAt!) == "2015-06-23 17:26:28", formatter.string(from: firstForum.updatedAt!))
            }
            expectation.fulfill()
        }
        XCTAssertNotNil(sessionTask)
        waitForExpectations(timeout: 300, handler: nil)
    }
    
    func testDownloadThreads() {
        let expectation = self.expectation(description: "")
        let sessionTask = ADaoDownloader.downloadThreads(for: 4, at: 1, completion: { (success, threads, error) in
            XCTAssert(threads?.count == 20)
            XCTAssertNotNil(threads?.first?.ID)
            XCTAssertNotNil(threads?.first?.userID)
            XCTAssertNotNil(threads?.first?.imageName)
            XCTAssertNotNil(threads?.first?.postDate)
            XCTAssertNotNil(threads?.first?.email)
            XCTAssertNotNil(threads?.first?.sage)
            XCTAssertNotNil(threads?.first?.admin)
            XCTAssertNotNil(threads?.first?.content)
            expectation.fulfill()
        })
        XCTAssertNotNil(sessionTask)
        waitForExpectations(timeout: 300, handler: nil)
    }
    
    func testDownloadReplies() {
        let expectation = self.expectation(description: "")
        let sessionTask = ADaoDownloader.downloadReplies(for: 10088110, at: 3, completion: { (success, thread, replies, error) in
            XCTAssert(thread?.replies?.count == 2)
            XCTAssert(thread?.ID == 10088110)
            XCTAssert(thread?.imageName == "2016-10-11/57fc49b38dc7c.png")
            XCTAssert(thread?.postDate == "2016-10-11(二)10:08:51")
            XCTAssert(thread?.userID == "jmxU5V7")
            XCTAssert(thread?.name == "无名氏")
            XCTAssert(thread?.email == "")
            XCTAssert(thread?.title == "无标题")
            XCTAssertNotNil(thread?.content)
            XCTAssert((thread?.responseCount)! >= 10)
            XCTAssertFalse(thread?.sage ?? true)
            XCTAssertFalse(thread?.admin ?? true)
            expectation.fulfill()
        })
        XCTAssertNotNil(sessionTask)
        waitForExpectations(timeout: 300, handler: nil)
    }
    
    func testDownloadQuote() {
        let expectation = self.expectation(description: "")
        let sessionTask = ADaoDownloader.downloadQuote(for: 10088110, completion: { (success, thread, error) in
            XCTAssert(thread?.ID == 10088110)
            XCTAssert(thread?.imageName == "2016-10-11/57fc49b38dc7c.png")
            XCTAssert(thread?.postDate == "2016-10-11(二)10:08:51")
            XCTAssert(thread?.userID == "jmxU5V7")
            XCTAssert(thread?.name == "无名氏")
            XCTAssert(thread?.email == "")
            XCTAssert(thread?.title == "无标题")
            XCTAssertNotNil(thread?.content)
            XCTAssertFalse(thread?.sage ?? true)
            XCTAssertFalse(thread?.admin ?? true)
            expectation.fulfill()
        })
        XCTAssertNotNil(sessionTask)
        waitForExpectations(timeout: 300, handler: nil)
    }
}
